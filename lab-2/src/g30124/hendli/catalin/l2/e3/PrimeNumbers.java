package g30124.hendli.catalin.l2.e3;

import java.util.Scanner;

public class PrimeNumbers {
	
	static void Prime(int x, int y){
		int i, j, nr = 0, c = 0;
		System.out.println("The prime numbers from [A,B] interval is:");
		for (i = x; i <= y; i++) {
             c = 0;
            for (j = 1; j <= i; j++) {
                    if (i % j == 0) {
                            c++;
                     }
                    }            			
            		if (c == 2) {
                    System.out.println(i);
                    nr++; 
                    }
            }
		System.out.println("And the no. of prime numbers is:");
		System.out.println(nr);
		
	}
	public static void main(String[] args){
	Scanner in = new Scanner(System.in);
    System.out.println("A= ");  int A = in.nextInt();
    System.out.println("B= ");  int B = in.nextInt();
    
    Prime(A,B);
    
	}
}
