package g30124.hendli.catalin.l2.e7;

import java.util.Scanner;
import java.util.Random;


public class GuessTheNumber {
	 
	public static void main(String[] args){
		   	
			Random rand = new Random();
			Scanner in = new Scanner(System.in);
			System.out.println("Hello! Can you guess the number? ");
			
			int n = in.nextInt(); 
			int  x = rand.nextInt(50) + 1;
			 
			int i = 0;
			 while (i <= 3) {
				if (n == x && i != 2) {
					System.out.println("You guessed it!:)");
					break;
					 }
				else if (n > x && i != 2) {
					System.out.println("Wrong answer, your number it too high.");
				
					}
				else if (n < x && i != 2) {
					System.out.println("Wrong answer, your number it too low.");
				    
					}
			    i++;
			    if(i == 3) {
			    	System.out.println("You lost.");
			        break;
			    }
			    n = in.nextInt();
	}
  }
}
