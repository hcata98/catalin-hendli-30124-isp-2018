package g30124.hendli.catalin.l2.e6;

import java.util.Scanner;

public class FactorialNum {

	static int recursive(int x)
	{
		if (x == 0) return 1;
		else 
			return x * recursive(x-1);
	}
	
	static void notrecursive(int x)
	{
		int i, p = 1;
		for (i = 0; i <= x; i++)
			p = p * x;
		System.out.println(x);
	}

	 public static void main(String[] args){
			Scanner in = new Scanner(System.in);
			System.out.println("N= ");  int n = in.nextInt();
			
			int x;
			x = recursive(n);
	  System.out.println(x); //computed using the recursive method
	  notrecursive(x); //computed using a nonrecursive method
	 }
}
