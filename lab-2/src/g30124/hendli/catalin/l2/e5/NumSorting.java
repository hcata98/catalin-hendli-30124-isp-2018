package g30124.hendli.catalin.l2.e5;

import java.util.Scanner;

public class NumSorting {
       
	static void sorting(int[] x) {

	    int n = x.length;
	    int i,j, aux;

	    for (i = 0;  i < n; i++) {
	        for (j = 1; j<(n - i); j++) {
	        	if (x[j - 1] > x[j]) {
	                aux = x[j - 1];
	                x[j - 1] = x[j];
	                x[j] = aux;
	            }
	        }
	    }
	}
	
	   public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.println("The 10 elements of the vector: ");  
		
		int a[]=new int[10];
		int i;
		
		for(i = 0; i < 10; i++){
			System.out.println("a["+i+"]= "); 
			a[i]= in.nextInt();
			}
		
		NumSorting b = new NumSorting();
		b.sorting(a);
		System.out.println("The numbers sorted: ");
		 for(i = 0; i < 10; i++)
		        System.out.print(a[i] + " ");
	}	
}

