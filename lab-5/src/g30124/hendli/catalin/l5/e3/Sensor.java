package g30124.hendli.catalin.l5.e3;

public abstract class Sensor {
    protected String location;

    public Sensor(String location) {
        this.location = location;
    }

    public int read(){
        return 0;
    }

    public String getLocation() {
        return location;
    }

}
