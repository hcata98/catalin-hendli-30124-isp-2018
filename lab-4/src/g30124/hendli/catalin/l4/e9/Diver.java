package g30124.hendli.catalin.l4.e9;

import becker.robots.*;
import javax.swing.*;

public class Diver extends Robot {
	
	
	public  Diver(City city, int a1, int a2, Direction dir) {
	super(city, a1, a2, dir);
	}
	
	public void dive() {
		for (int i = 0 ;i < 12 ; i++)
		super.turnLeft();
	}

public static void main(String[] args) {
		
		City Cluj = new City();
		Wall blockAve0 = new Wall(Cluj, 2, 1, Direction.NORTH);
		Wall blockAve1 = new Wall(Cluj, 2, 0, Direction.EAST);
		Wall blockAve2 = new Wall(Cluj, 3, 0, Direction.EAST);
		Wall blockAve3 = new Wall(Cluj, 4, 0, Direction.EAST);
		Wall blockAve4 = new Wall(Cluj, 4, 1, Direction.WEST);
		Wall blockAve5 = new Wall(Cluj, 4, 1, Direction.SOUTH);
		Wall blockAve6 = new Wall(Cluj, 4, 2, Direction.SOUTH);
		Wall blockAve7 = new Wall(Cluj, 4, 2, Direction.EAST);
		
		Robot karel = new Robot(Cluj, 1, 1, Direction.NORTH);
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		((Diver)karel).dive();
		karel.move();
		karel.move();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
	}
}

