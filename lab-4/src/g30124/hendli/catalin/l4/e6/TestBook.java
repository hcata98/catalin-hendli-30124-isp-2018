package g30124.hendli.catalin.l4.e6;
import g30124.hendli.catalin.l4.e4.Author;

import static org.junit.Assert.*;
import org.junit.*;

public class TestBook {
	private Author[] a;
	private Book b;
	
	@Before
	public void initTest() {
		System.out.println("Initialize test.");
	}
	
	@Test
	public void TestBookNameAndAuthor() {
		Author[] a = new Author[1];
		a[0] = new Author("Jamie Andrew", "dotsdots@yahoo.com",'m');
		b = new Book("Book 1",a,120);
		assertEquals("Book 1",b.getName());
		assertEquals("Jamie Andrew (m) dotsdots@yahoo.com",a[0].toString());
		//here i used the toString() method from Author class for testing
		// getName(), getEmail(), getGender() could also be used
	}
	
	@Test
	public void TestSettersAndGetters() {
		b = new Book("Book 2",a,130,35);
		b.setPrice(23.99);
		assertTrue(b.getPrice() == 23.99);
		b.setQtyInStock(10);
		assertEquals(10,b.getQtyInStock());
	}
	
	@Test
	public void TestToString() {
		Author[] a = new Author[1];
		a[0] = new Author("Jamie Andrew", "dotsdots@yahoo.com",'m');
		b = new Book("Book 3",a,100,20);
		assertEquals("Book 3 by 1 authors",b.toString());
	}
	
	@After
	public void finTest() {
		System.out.println("Finished test.");
	}
}
