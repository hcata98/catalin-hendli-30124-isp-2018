package g30124.hendli.catalin.l4.e6;

import g30124.hendli.catalin.l4.e4.Author;

public class Book {
	private String name;
	private Author[] authors;
	private double price;
	private int qtyInStock = 0;
	
	public Book(String name, Author[] authors, double price) {
		this.name    = name;
	    this.authors  = authors;
	    this.price   = price;
	}
	
	public Book(String name, Author[] authors, double price, int qtyInStock) {
		this.name = name;
	    this.authors = authors;
	    this.price = price;
	    this.qtyInStock = qtyInStock;
	}
	
	public String getName() {
		return name;
	}
	
	public Author[] getAuthors() {
		return authors;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock= qtyInStock;
	}
		
	public String toString() {
		int i, n= 0;
		for (i = 0 ; i < getAuthors().length; i++)
			n++;
		return name + " by " + n + " authors";
	}
		
	  static void printAuthors(Author[] aut) {
		  for (int i = 0; i < aut.length; i++) {
		         System.out.print(aut[i].getName());
		         System.out.print("\n");
		}
	}
	  
//Test for compilator
	public static void main(String[] args) {
		
		Author[] aut = new Author[3];
		aut[0] = new Author("Andreea Joko", "Andreea@somewhere.com", 'f');
		aut[1] = new Author("Paul Smith", "Paul@nowhere.com", 'm');
		aut[2] = new Author("Robert Galdo", "Robert@something.it", 'm');
				
	   Book book = new Book("Key of Success", aut, 29.99, 10);
	   System.out.println(book); ;  //Test Book's toString()
	   printAuthors(aut);
		
	   //Test Getters and Setters
		book.setPrice(23.99);
		book.setQtyInStock(9);
		System.out.println("Name is: " + book.getName());
		System.out.println("Price is: " + book.getPrice());
		System.out.println("Qty is: " + book.getQtyInStock());
		System.out.println("One of the authors is: " + aut[0].toString());  // First Author's toString()
	}
}