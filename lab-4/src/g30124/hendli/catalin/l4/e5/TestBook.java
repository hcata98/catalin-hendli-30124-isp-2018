package g30124.hendli.catalin.l4.e5;
import g30124.hendli.catalin.l4.e4.Author;

import static org.junit.Assert.*;
import org.junit.*;


public class TestBook {
	private Book b;
	private Author a;
	
	@Before
	public void initTest() {
		System.out.println("Initialize test.");
	}
	
	@Test
	public void TestGetAuthor() {
		a = new Author("Jamie Andrew", "dotsdots@yahoo.com",'m');
		b = new Book("Book no. 1",a,120);
		assertEquals("Book no. 1",b.getName());
		assertEquals("Jamie Andrew (m) dotsdots@yahoo.com",b.getAuthor().toString());
		//here i used the toString() method from Author class for testing
		//i could also use getName(), getEmail(), getGender();
	}
	
	@Test
	public void TestSettersAndGetters() {
		b = new Book("Book no.2",a,23.99,20);
		b.setPrice(20.99);
		assertTrue(b.getPrice() == 20.99);
		b.setQtyInStock(23);
		assertEquals(23,b.getQtyInStock());
	}
	
	@Test
	public void TestToString() {
		a = new Author("Jamie Andrew", "dotsdots@yahoo.com",'m');
		b = new Book("Book no.5",a,200,40);
		assertEquals("Book no.5 by Jamie Andrew (m) at dotsdots@yahoo.com",b.toString());
	}
	
	@After
	public void finTest() {
		System.out.println("Finished test.");
	}
}
