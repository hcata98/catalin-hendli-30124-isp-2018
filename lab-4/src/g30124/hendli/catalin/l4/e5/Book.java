package g30124.hendli.catalin.l4.e5;

import g30124.hendli.catalin.l4.e4.Author;

public class Book {
	private String name;
	private Author author;
	private double price;
	private int qtyInStock = 0;
	
	public Book(String name, Author author, double price) {
		this.name    = name;
	    this.author  = author;
	    this.price   = price;
	}
	
	public Book(String name, Author author, double price, int qtyInStock) {
		this.name = name;
	    this.author = author;
	    this.price = price;
	    this.qtyInStock = qtyInStock;
	}
	
	public String getName() {
		return name;
	}
	
	public Author getAuthor() {
		return author;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock= qtyInStock;
	}
	
	public String toString() {
		return name + " by " + author.getName() + " (" + author.getGender() + ") " + "at "
		+ author.getEmail();
	}
	
	//Test for compilator
	public static void main(String[] args) {

		Book b1 = new Book("Book Thief", new Author("NameAuthor", "address@something.com", 'f'),
				23.99, 20);  
		System.out.println(b1);  //Test Book's toString()

		//Test Getters and Setters
		b1.setPrice(20.99);
		b1.setQtyInStock(23);
		System.out.println("Name is: " + b1.getName());
		System.out.println("Price is: " + b1.getPrice());
		System.out.println("Qty is: " + b1.getQtyInStock());
		System.out.println("Author's name is: " + b1.getAuthor().getName());
		System.out.println("Author's email is: " + b1.getAuthor().getEmail());
		System.out.println("Author's all infos: " + b1.getAuthor());  // Author's toString()
		}
}
