package g30124.hendli.catalin.l4.e4;

public class Author {

	private String name;
	private String email;
	private char gender;
	
	public Author(String name, String email, char gender) {
		this.name = name;
		this.email = email;
		this.gender = gender;
		}
	
	public String getName() {
		return  name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return  email;
	}
	
	public char getGender() {
		return gender;
	}
	
	public String toString() {
		return name + " (" + gender + ") " + email;
	}
	
	//Test for compilator
	public static void main(String[] args) {
		
		Author a0 = new Author("James Dickens","name@something.com",'m');
		
		System.out.println(a0.getName()); // name
		System.out.println(a0.getEmail()); //email
		System.out.println(a0.getGender()); //gender
		System.out.println(a0.toString()); // toString()
	}
}
