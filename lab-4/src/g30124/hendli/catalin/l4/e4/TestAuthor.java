package g30124.hendli.catalin.l4.e4;

import static org.junit.Assert.*;
import org.junit.*;

public class TestAuthor {
	
	private Author a;
	
	@Before
	public void initTest() {
		System.out.println("Initialize test.");
	}
		
	@Test
	public void TestGetters() {
		a = new Author("Name", "name@host.com", 'm');
		a.setEmail("newemail@yahoo.com");
		assertEquals("newemail@yahoo.com",a.getEmail());
		assertEquals("Name",a.getName());
		a = new Author("Name", "whatever", 'f');
		assertEquals('f',a.getGender());
		assertEquals('f',a.getGender());
	}
	
	@Test
	public void TestToString() {
		a = new Author("Name", "name@host.com", 'f');
		assertEquals("Name (f) name@host.com",a.toString());
	}
	
	@After
	public void finTest() {
		System.out.println("Finished test.");
	}
}
