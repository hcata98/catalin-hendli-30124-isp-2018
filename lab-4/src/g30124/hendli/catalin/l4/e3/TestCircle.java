package g30124.hendli.catalin.l4.e3;

import static org.junit.Assert.*;
import org.junit.*;

public class TestCircle {

	private Circle c; 
	
	@Before
	public void initTest(){
		System.out.println("Initialize test.");
	}

	@Test
	public void TestGetters(){
		c = new Circle(2);
		assertTrue(c.getRadius() == 2);
	    c = new Circle();
	    assertTrue(c.getArea() == 3.14); // for pi = 3.14 and radius = 1 --> Area is 3.14
	}
	
	@After
	public void finTest(){
		System.out.println("Finished test.");
	}
}
