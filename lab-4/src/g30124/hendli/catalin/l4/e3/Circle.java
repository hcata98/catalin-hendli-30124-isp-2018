package g30124.hendli.catalin.l4.e3;

public class Circle {

	private double radius = 1.0;
	private String color = "red";
	
	
	 public Circle(){
	 }

	 public Circle(double radius){
		 this.radius = radius;
	 }
	 
	 public double getRadius(){
	        return radius;
	 }
	 
	 public double getArea(){
		 final double pi = 3.14;
	        return pi * radius * radius;
	 }

//Test for compilator
	 public static void main(String[] args){
	    
        Circle c =  new Circle();
	    
	    System.out.println("Radius is " + c.getRadius());
	    System.out.println("Area is " + c.getArea());
  }
}
