package g30124.hendli.catalin.l4.e8;

public class Shape {
	
	private String color = "red";
	private boolean filled = true;
	
	public Shape() {
		this.color = "green";
		this.filled = true;
	}
	
	public Shape(String color, boolean filled) {
		this.color = color;
		this.filled = filled;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public boolean isFilled() {
		return filled;
	}
	
	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	public String toString() {
		if (filled == true)
		return "A shape of color " + color + " and filled";
		else 
		return "A shape of color " + color + " and not filled";
	}

//Test for compilator
	public static void main(String args[]) {
		
		Shape s1 = new Shape ();
		System.out.println(s1.toString());
		
		Shape s2 = new Shape("blue",true);
		s2.setColor("purple");
		s2.setFilled(false);
		System.out.println(s2.toString());
		System.out.println(s2.getColor());
		System.out.println(s2.isFilled());
	}
}
