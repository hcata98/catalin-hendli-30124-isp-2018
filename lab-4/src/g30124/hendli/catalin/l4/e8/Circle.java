package g30124.hendli.catalin.l4.e8;

public class Circle extends Shape {
	
	private double radius = 1.0;
	
	public Circle() {
		this.radius = 1.0;
	 }
	
	public Circle(double radius) {
		 this.radius = radius;
	 }
	
	public Circle(double radius, String color, boolean filled) {
		super(color, filled);
		this.radius = radius;
	}
       
	public double getRadius() {
	     return radius;
	 }
	 
	public void setRadius(double radius) {
	     this.radius = radius;
	 }
	
	public double getArea() {
		final double pi = 3.14;
		 
		return pi * radius * radius;
	 }
	
	public double getPerimeter() {
		final double pi = 3.14;
		 
		return  2* pi * radius;
	 }
	
	@Override
	public String toString() {
		return "A circle with radius = " + radius + ", which is a subclass of " + super.toString() ;
	}
	
	public static void main(String[] args) {
		Shape s2 = new Shape("red",false);
		Circle c1 = new Circle();
		Circle c2 = new Circle(2.5);
		Circle c3 = new Circle(3, "red", false);
		
		System.out.println(c3.toString());
		System.out.println(c1.toString());
		System.out.println(c1.getRadius());
		c2.setRadius(4.0); 
		System.out.println(c2.getPerimeter()); // 4 * 2 * 3.14 = 25.12
		System.out.println(c2.getArea()); // 3,14 * 4* 4 = 50.24
	}
}
