package g30124.hendli.catalin.l4.e8;

import static org.junit.Assert.*;
import org.junit.*;


public class TestClass {
	
	@Before
	public void initTest() {
		System.out.println("Initialize test.");
	}
	
	@Test
	public void TestShape() {
		Shape s1 = new Shape("blue", true);
		s1.setColor("white");
		s1.setFilled(false);
		assertEquals("A shape of color white and not filled", s1.toString());
		Shape s2 = new Shape();
		assertEquals("green",s2.getColor());
		assertEquals(true, s2.isFilled());
	}
	
	@Test
	public void TestCircle() {
		Circle c1 = new Circle();
		assertTrue(c1.getArea() == 3.14);
		assertTrue(c1.getPerimeter() == 6.28);
		Circle c2 = new Circle(3.5);
		c2.setRadius(4);
		assertTrue(c2.getRadius() == 4);
		Circle c3 = new Circle(3.33, "blue", true);
		assertEquals("A circle with radius = 3.33, which is a subclass of A shape of color blue "  + 
	   "and filled",c3.toString());
	}
	
	@Test
	public void TestRectangle() {
		Rectangle r1 = new Rectangle();
		assertTrue(r1.getWidth() == 1);
		assertTrue(r1.getLength() == 1);
		Rectangle r2 = new Rectangle(3,4);
		r2.setWidth(5);
		r2.setLength(2);
		assertTrue(r2.getArea() == 10);
		assertTrue(r2.getPerimeter() == 14);
		Rectangle r3 = new Rectangle(3, 5, "orange", false);
		assertEquals("A rectangle with width = 3.0 and length = 5.0 " + "which is a subclass of A shape "
		+ "of color orange and not filled",r3.toString());
	}
	
	public void TestSquare() {
		Square sq1 = new Square();
		assertEquals("A square with side = 1.0 which is a subclass of A rectangle with width = 1.0 and "
	    + "length = 1.0 which is a subclass of A shape of color green and filled",sq1.toString());
		Square sq2 = new Square(5, "yellow", true);
		assertTrue(sq2.getSide(5) == 5);
		sq2.setWidth(12);
		sq2.setLength(10);
		sq2.setSide(4);
		assertEquals("A square with side = 4.0 which is a subclass of A rectangle with width = 12.0 and "
		+ "length = 10.0 which is a subclass of A shape of color green and filled",sq1.toString());
	}
	
	@After
	public void finTest() {
		System.out.println("Finished test.");
	}
}
