package g30124.hendli.catalin.l4.e8;

public class Square extends Rectangle {

	private double side; 
	
	public Square() {
		super(length,width);
	}
	
	public Square(double side) {
		super(length,width);
		this.side = side;
		
	}
	
	public Square(double side, String color, boolean filled) {
		super(length, width,color,filled);
		if (side <= length && side <= width)
			this.side = side;
	}
	
	public double getSide(double side) {
		if (side <= length && side <= width)
		  if (length == width)
			  side = length;
		return side;
	}
	
	public void setSide(double side) {
		this.side = side;
	}
	
	@Override
	public void setWidth(double width) {
		super.width = width;
	}
	
	@Override
	public void setLength(double length) {
		super.length = super.getWidth();
	}
	
	@Override
	public String toString() {
		return "A square with side = " + getSide(side) +  ", which is a subclass of " + super.toString() ;
	}

//Test for compilator
	public static void main(String[] args) {
		Rectangle r = new Rectangle();
		Square sq1 = new Square(1);
		Square sq2 = new Square(2, "yellow", false);
		
		System.out.println(sq1.toString());
		System.out.println(sq2.toString());
	}
}
