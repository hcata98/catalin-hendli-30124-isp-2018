package g30124.hendli.catalin.l4.e7;

import g30124.hendli.catalin.l4.e3.Circle;

public class Cylinder extends Circle { 
	private double height = 1.0;  
	   
	public Cylinder() {
	      super();       
	   }

	public Cylinder(double radius) {
	      super(radius);      
	   }

	public Cylinder(double radius, double height) {
	      super(radius);  
	      this.height = height;
	   }
	   
	public double getHeight() {
	      return height; 
	   }
	  
	public double getVolume() {
	      return getArea()*height; 
	   }
	   
//Test for Compilator
	public static void main(String[] args){
		Cylinder c1 = new Cylinder(); // 0 arguments
	      System.out.println("Cylinder: " + " radius = " + c1.getRadius() + " height = " + c1.getHeight()
	 + " Area(base) = " + c1.getArea() + " volume = " + c1.getVolume());
	    Cylinder c2 = new Cylinder(10.0); // radius
	      System.out.println("Cylinder: " + " radius = " + c2.getRadius() + " height = " + c2.getHeight()
	 + " Area(base) = " + c2.getArea() + " volume = " + c2.getVolume());
	    Cylinder c3 = new Cylinder(2.0, 10.0); // radius + height
	      System.out.println("Cylinder: " + " radius = " + c3.getRadius() + " height = " + c3.getHeight()
	 + " Area(base) = " + c3.getArea() + " volume = " + c3.getVolume());  
	}
}