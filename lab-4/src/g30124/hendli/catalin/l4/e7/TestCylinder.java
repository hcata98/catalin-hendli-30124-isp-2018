package g30124.hendli.catalin.l4.e7;
import g30124.hendli.catalin.l4.e3.Circle;

import static org.junit.Assert.*;
import org.junit.*;


public class TestCylinder {
	private Cylinder c;
	
	@Before
	public void initTest() {
		System.out.println("Initialize test.");
	}
	
	@Test
	public void TestRadius() {
		c = new Cylinder();
		assertTrue(c.getRadius() == 1);
	}

	@Test
	public void TestArea() {
	    c = new Cylinder(2);
	    assertTrue(c.getArea() == 12.56); // for pi = 3.14 and radius = 2 --> Area is 12.56
	 }
	
	@Test
	public void TestGetHeightAndVolume() {
		 c = new Cylinder(3,4);
		 assertTrue(c.getHeight() == 4);
		 assertTrue(c.getVolume() == 4*c.getArea());
	}
	
	@After
	public void finTest() {
		System.out.println("Finished test.");
	}
}