package g30124.hendli.catalin.l3.e3;

import becker.robots.*;

public class MoveAndTurnAround {
	 public static void main(String[] args)
	   {
		   City cc = new City();
		   Robot rr = new Robot(cc, 1, 1, Direction.NORTH);
		   
		   for (int i = 0 ; i < 5 ;i ++)
			    rr.move();
		   
		   rr.turnLeft();
		   rr.turnLeft();
		  
		   
		   for (int i = 0 ; i < 5 ;i ++)
			    rr.move();
		      
	}
}
