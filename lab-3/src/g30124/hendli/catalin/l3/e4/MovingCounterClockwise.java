package g30124.hendli.catalin.l3.e4;

import becker.robots.*;

public class MovingCounterClockwise {
	public static void main(String[] args)
	   {
	      
	      City Ct = new City();
	      Wall blockAve0 = new Wall(Ct, 1, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(Ct, 2, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(Ct, 2, 2, Direction.EAST);
	      Wall blockAve3 = new Wall(Ct, 1, 2, Direction.EAST);
	      Wall blockAve4 = new Wall(Ct, 1, 2, Direction.NORTH);    
	      Wall blockAve5 = new Wall(Ct, 1, 1, Direction.NORTH);
	      Wall blockAve6 = new Wall(Ct, 2, 1, Direction.SOUTH);
	      Wall blockAve7 = new Wall(Ct, 2, 2, Direction.SOUTH);
	      
	      Robot Robot = new Robot(Ct, 0, 2, Direction.WEST);
	      
	      int i;
	    // for (int j = 0 ; j < 1000 ; j ++) { -> for multiple rotations
	      for(i = 0 ; i < 2; i++)
			   Robot.move();
		   Robot.turnLeft();
		   for(i = 0 ; i < 3; i++)
			   Robot.move();
		   Robot.turnLeft();
		   
		   for(i = 0 ; i < 3; i++)
			   Robot.move();
		   Robot.turnLeft();
		   
		   for(i = 0 ; i < 3; i++)
			   Robot.move();
		  
		   Robot.turnLeft();
		   Robot.move();
	 // }
	   }
}
