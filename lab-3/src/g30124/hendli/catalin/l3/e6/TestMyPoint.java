package g30124.hendli.catalin.l3.e6;

public class TestMyPoint {
	public static void main  (String[] args){
		
		MyPoint p1 = new MyPoint();  
		System.out.println("1)Create the point p1 at the origins: ");
		System.out.println(p1);     
		System.out.println("Set coordinates x and y: ");
		p1.setX(3);   
		p1.setY(4);
		System.out.println("x = " + p1.getX());  
		System.out.println("y = " + p1.getY());
		System.out.println("Set  x and y for point p1 :");
		p1.setXY(3, 0);   
	    System.out.println(p1);
	    System.out.println("---------");
		MyPoint p2 = new MyPoint(0, 4);  
	    System.out.println("2)Create point p2");
		System.out.println(p2); // Here is the use of the ToString method
		System.out.println("The distance between p1 and p2 is:");
		System.out.println(p1.distance(p2));    
		System.out.println("The distance between p2 and (5,6) is:");
		System.out.println(p2.distance(5, 4));  

 
}
}
