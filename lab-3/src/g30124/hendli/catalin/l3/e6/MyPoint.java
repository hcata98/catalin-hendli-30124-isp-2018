package g30124.hendli.catalin.l3.e6;

import java.util.Scanner;

public class MyPoint {

    private int x,y;
    
    public MyPoint(){
    		this(0,0);
    	}


    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

   
    public void setX(int x) {
        this.x = x;
    }
   
    public int getX() {
        return this.x;
    }


    public void setY(int y) {
        this.y = y;
    }
    
    public int getY() {
        return this.y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance(int x, int y) {
        int xDiff = this.x - x;
        int yDiff = this.y - y;
        return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
    }

    public double distance(MyPoint another) {
        int xDiff = this.x - another.getX();
        int yDiff = this.y - another.getY();
        return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

}   