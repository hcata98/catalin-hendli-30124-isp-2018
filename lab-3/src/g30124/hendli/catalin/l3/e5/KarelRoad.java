package g30124.hendli.catalin.l3.e5;

import becker.robots.*;

public class KarelRoad {

	public static void main(String[] args)
	   {
	      
	      City Ct = new City();
	      Thing parcel = new Thing(Ct, 2, 2);
	      Robot karel = new Robot(Ct, 1, 2, Direction.SOUTH);
	      Wall blockAve0 = new Wall(Ct, 1, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(Ct, 2, 1, Direction.WEST);
	      Wall blockAve3 = new Wall(Ct, 1, 2, Direction.EAST);
	      Wall blockAve4 = new Wall(Ct, 1, 2, Direction.NORTH);    
	      Wall blockAve5 = new Wall(Ct, 1, 1, Direction.NORTH);
	      Wall blockAve6 = new Wall(Ct, 2, 1, Direction.SOUTH);
	      Wall blockAve7 = new Wall(Ct, 1, 2, Direction.SOUTH);
	      
	      int i;
	   
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.pickThing();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	     for (i = 0; i < 3 ; i++)
	    	 karel.turnLeft();
	      karel.move();
	      for (i = 0; i < 3 ; i++)
		      karel.turnLeft();
	      karel.move();
	      for (i = 0; i < 3 ; i++)
		      karel.turnLeft();
	     }
}
