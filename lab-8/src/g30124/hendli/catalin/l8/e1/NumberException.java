package g30124.hendli.catalin.l8.e1;

public class NumberException extends Exception {
    int nr;
    public NumberException(int nr,String msg) {
        super(msg);
        this.nr = nr;
    }
    public int getNr ()
    {
        return nr;
    }

}
