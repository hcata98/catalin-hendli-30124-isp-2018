package g30124.hendli.catalin.l8.e4;


public class GarageCapacity extends Exception {
    int nr;
    public GarageCapacity(String msj, int nr) 
    {
        super(msj);
        this.nr=nr;
    }

    public int getNr() {
        return nr;
    }
}
