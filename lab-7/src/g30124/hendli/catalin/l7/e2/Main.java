package g30124.hendli.catalin.l7.e2;

import  g30124.hendli.catalin.l7.e1.BankAccount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
       Bank bank=new Bank();

       bank.addAccount("Andrei",300);
       bank.addAccount("Mihai",300.3);
       bank.addAccount("Bogdan",1000);
       
       bank.printAccounts();
        System.out.println("-----");

        ArrayList<BankAccount> bankAccounts= bank.getAccounts();

        Collections.sort(bankAccounts,BankAccount.BankAccountComparator);
        for(BankAccount b:bankAccounts)
            System.out.println(b.toString());

    }
}
